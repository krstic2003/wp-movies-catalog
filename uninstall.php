<?php

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    die;
}

$movies = get_posts( 
        array(
                'post_type' => 'movies',
                'numberposts' => -1
            ) 
        );
foreach ($movies as $movie) {
  wp_delete_post( $movie->ID, true );
}

$genres = get_terms(
    array(
        'taxonomy' => 'kmcategory',
        'hide_empty' => false,
    )
);

foreach ($genres as $genre) {
  wp_delete_term( $genre->term_id, 'kmcategory' );
}