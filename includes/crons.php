<?php

add_filter( 'cron_schedules', 'kmovie_custom_schedules' );
function kmovie_custom_schedules( $schedules ) {
    $schedules['kmovie_every_day'] = array(
        'interval' => 60 * 60 * 24,
        'display'  => __( 'Every day', 'kmovies-plugin' )
    );
    return $schedules;
}

// Schedule an action if it's not already scheduled
if ( ! wp_next_scheduled( 'kmovie_genre_import' ) ) {
    wp_schedule_event( strtotime('00:00:01'), 'kmovie_every_day', 'kmovie_genre_import' );
}

if ( ! wp_next_scheduled( 'kmovie_movie_import' ) ) {
    wp_schedule_event( strtotime('00:30:01'), 'kmovie_every_day', 'kmovie_movie_import' );
}

// Hook into the actions that'll fire every day
add_action( 'kmovie_genre_import', 'kmovie_import_event_func' );
add_action( 'kmovie_movie_import', 'kmovie_import_event_func' );
function kmovie_import_event_func() {
    if ( current_action() === 'kmovie_genre_import' ) {
        kmovies_import_genres();
    } elseif ( current_action() === 'kmovie_movie_import' ) {
        kmovies_import_movies();
    }
}