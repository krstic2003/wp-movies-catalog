<?php


// Add admin menu item
add_action('admin_menu', 'kmovies_menu_item');
 
function kmovies_menu_item()
{
    add_menu_page('Movies Catalog', 'Movies Catalog', 'administrator', 'kmovies_page', 'kmovies_settings', 'dashicons-list-view');

    //call register settings function
    add_action( 'admin_init', 'register_kmovies_settings' );

}


function register_kmovies_settings() {
	//register plugin settings
	register_setting( 'kmovies-settings-group', 'kmovies_mdb_key' );

}


function kmovies_settings() {
?>
<div class="wrap">
<h1>Movies Catalog - Options Page</h1>

<form method="post" action="options.php">
    <?php 
    settings_fields( 'kmovies-settings-group' ); 
 	do_settings_sections( 'kmovies-settings-group' ); 

     $import_url = get_site_url().'/wp-json/kmovies/v1/import-movies';
     $import_url_genres = get_site_url().'/wp-json/kmovies/v1/import-genres';
    ?>
    <div class="kmovies-table">
        <h3><?php echo __('API', 'kmovies-plugin'); ?></h3>
        <div class="f-row-setting">
            <?php echo __('API key from themoviedb', 'kmovies-plugin'); ?>
            
            <input type="text" 
                name="kmovies_mdb_key" 
                value="<?php echo sanitize_option( 'kmovies_mdb_key', get_option('kmovies_mdb_key') ); ?>" 
            />
            <small>
                <?php echo __('Generate key here -', 'kmovies-plugin'); ?> 
                <a href="https://www.themoviedb.org/ target="_blank">themoviedb.org</a>
            </small>
        </div>  
        <h3><?php echo __('Shortcodes', 'kmovies-plugin'); ?></h3>
        <div class="f-row-setting">
            <?php echo __('Email Form Shortcode', 'kmovies-plugin'); ?> - 
            <span id="shortcode-for-copy">[kmovies_form]</span> 
            <a href="#" id="copy-shortcode"><?php echo __('Copy', 'kmovies-plugin'); ?></a>
        </div>
        <div class="f-row-setting">
            <?php echo __('List Movies Shortcode', 'kmovies-plugin'); ?> - 
            <span id="shortcode-for-copy-list">[kmovies_list]</span> 
            <a href="#" id="copy-shortcode-list"><?php echo __('Copy', 'kmovies-plugin'); ?></a>
        </div>
        <h3><?php echo __('Import Genres from themoviedb', 'kmovies-plugin'); ?></h3>
        <div class="f-row-setting">
            <a href="<?php echo esc_url( $import_url_genres ); ?>" target="_blank"><?php echo __('Run Importer', 'kmovies-plugin'); ?></a>
        </div>
        <div style="color:red; font-size:18px; font-weight:bold"><?php echo __('Import Generes before movies please!', 'kmovies-plugin'); ?></div>
        <h3><?php echo __('Import Movies from themoviedb', 'kmovies-plugin'); ?></h3>
        <div class="f-row-setting">
            <a href="<?php echo esc_url( $import_url ); ?>" target="_blank"><?php echo __('Run Importer', 'kmovies-plugin'); ?></a>
        </div>
        
    </div>
    
    <?php submit_button(); ?>

</form>
</div>
<?php }