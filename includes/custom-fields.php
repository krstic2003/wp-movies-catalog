<?php

// Add custom fields to Movies post type
function add_kmovies_meta_boxes() {
    add_meta_box(
        "post_metadata_kmovies_post",
        __( 'Movie Custom Fields', 'kmovies-plugin' ),
        "kmovies_render_custom_fields",
        "kmovies",
        "normal",
        "high",
    );

    add_meta_box(
		'post_custom_gallery',
		__( 'Movie Gallery', 'kmovies-plugin' ),
		'kmovies_render_gallery_field',
		'kmovies',
		'normal',
		'high'
	);

    add_meta_box(
		'post_custom_generes',
		__( 'Movie Genres', 'kmovies-plugin' ),
		'kmovies_render_generes_field',
		'kmovies',
		'normal',
		'high'
	);
}
add_action( "admin_init", "add_kmovies_meta_boxes" );

// Save custom fields
function save_post_meta_boxes(){
    global $post;
    
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }
    if ( !isset( $_POST['post_type'] ) || 'kmovies' != $_POST['post_type'] ) {
        return;
    }
    
    if ( !wp_verify_nonce( $_REQUEST['_movie_fields_process'], '_movie_fields_nonce' ) ) {
        return;
    }

    $post_id = $post->ID;

    if ( !current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

    if( isset($_POST[ "_movie_genre" ]) ){
        update_post_meta( $post_id, "_movie_genre", $_POST[ "_movie_genre" ] );
        wp_set_post_terms( $post_id,  $_POST[ "_movie_genre" ], 'kmcategory', false ); 
    }

    update_post_meta( $post_id, "_movie_trailer", sanitize_text_field( $_POST[ "_movie_trailer" ] ) );
    update_post_meta( $post_id, "_movie_raiting", sanitize_text_field( $_POST[ "_movie_raiting" ] ) );
    update_post_meta( $post_id, "_movie_cast", sanitize_text_field( $_POST[ "_movie_cast" ] ) );
    update_post_meta( $post_id, "_movie_release", sanitize_text_field( $_POST[ "_movie_release" ] ) );

    


    if(!$_POST['gallery']){
        delete_post_meta( $post_id, 'gallery_data' );
    }

    // Build array for saving post meta
    $gallery_data = array();
    for ($i = 0; $i < count( $_POST['gallery']['image_url'] ); $i++ ){
        if ( '' != $_POST['gallery']['image_url'][$i]){
            $gallery_data['image_url'][]  = $_POST['gallery']['image_url'][ $i ];
        }
    }

    if(!$gallery_data){
        delete_post_meta( $post_id, 'gallery_data' );
    }

    update_post_meta( $post_id, 'gallery_data', $gallery_data );

}
add_action( 'save_post', 'save_post_meta_boxes' );

// callback function to render fields in Movies post type
function kmovies_render_custom_fields(){
    global $post;
    $custom = get_post_custom( $post->ID );
    $movieTrailer = "";
    $movieRaiting = "";
    $movieCast = "";
    $movieReleaseDate = "";
    $movieGallery = "";
    if ( isset( $custom[ "_movie_trailer" ] ) ) {   
        $movieTrailer = $custom[ "_movie_trailer" ][ 0 ];
    }
    if ( isset( $custom[ "_movie_raiting" ] ) ) {   
        $movieRaiting = $custom[ "_movie_raiting" ][ 0 ];
    }

    if ( isset( $custom[ "_movie_cast" ] ) ) {   
        $movieCast = $custom[ "_movie_cast" ][ 0 ];
    }

    if ( isset( $custom[ "_movie_release" ] ) ) {   
        $movieReleaseDate = $custom[ "_movie_release" ][ 0 ];
    }

    echo '<div class="f-row">';
    echo '<label for="movie_trailer">'.__( 'Movie Trailer', 'kmovies-plugin' ).':</label> ';
    echo '<input type="text" id="movie_trailer" name="_movie_trailer" value="'.$movieTrailer.'" placeholder="'.__( 'Movie Trailer', 'kmovies-plugin' ).'">';
    echo '</div>';

    echo '<div class="f-row">';
    echo '<label for="movie_raiting">'.__( 'Movie Raiting', 'kmovies-plugin' ).':</label> ';
    echo '<input type="text" id="movie_raiting" name="_movie_raiting" value="'.$movieRaiting.'" placeholder="'.__( 'Movie Raiting', 'kmovies-plugin' ).'">';
    echo '</div>';

    echo '<div class="f-row">';
    echo '<label for="movie_cast">'.__( 'Movie Cast', 'kmovies-plugin' ).':</label> ';
    echo '<input type="text" id="movie_cast" name="_movie_cast" value="'.$movieCast.'" placeholder="'.__( 'Movie Cast', 'kmovies-plugin' ).'">';
    echo '</div>';

    echo '<div class="f-row">';
    echo '<label for="movie_release">'.__( 'Movie Release Date', 'kmovies-plugin' ).':</label> ';
    echo '<input type="date" id="movie_release" name="_movie_release" value="'.$movieReleaseDate.'" placeholder="'.__( 'Movie Release Date', 'kmovies-plugin' ).'">';
    echo '</div>';

    wp_nonce_field( '_movie_fields_nonce', '_movie_fields_process' );
}

// callback function to render Gallery in Movies post type
function kmovies_render_gallery_field(){
	global $post;
	$gallery_data = get_post_meta( $post->ID, 'gallery_data', true );
	?>
	<div id="gallery_wrapper">
		<div id="img_box_container">
		<?php 
		if ( isset( $gallery_data['image_url'] ) ){
			for( $i = 0; $i < count( $gallery_data['image_url'] ); $i++ ){
			?>
			<div class="gallery_single_row dolu">
                <div class="gallery_area image_container ">
                    <img class="gallery_img_img" 
                        src="<?php esc_html_e( $gallery_data['image_url'][$i] ); ?>" 
                        height="55" 
                        width="55" 
                        onclick="open_media_uploader_image_this(this)"
                    />
                    <input type="hidden"
                            class="meta_image_url"
                            name="gallery[image_url][]"
                            value="<?php esc_html_e( $gallery_data['image_url'][$i] ); ?>"
                    />
                </div>
                <div class="gallery_area">
                    <span class="button remove" onclick="remove_img(this)" title="<?php echo __( 'Remove', 'kmovies-plugin' ); ?>">X</span>
                </div>
                <div class="clear"></div> 
			</div>
			<?php
			}
		}
		?>
		</div>
		<div style="display:none" id="master_box">
			<div class="gallery_single_row">
				<div class="gallery_area image_container" onclick="open_media_uploader_image(this)">
					<input class="meta_image_url" value="" type="hidden" name="gallery[image_url][]" />
				</div> 
				<div class="gallery_area"> 
					<span class="button remove" onclick="remove_img(this)" title="<?php echo __( 'Remove', 'kmovies-plugin' ); ?>">X</span>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div id="add_gallery_single_row">
		  <input class="button add" type="button" value="+" onclick="open_media_uploader_image_plus();" title="<?php echo __( 'Add image', 'kmovies-plugin' ); ?>"/>
		</div>
	</div>
	<?php
    wp_nonce_field( '_movie_fields_nonce', '_movie_fields_process' );
}

function kmovies_render_generes_field(){
    global $post;

    $current = get_the_terms( $post->ID, 'kmcategory' );
    $currentIds = [];
    if(empty($current)) {
        $current = [];
    }
    foreach ($current as $term) {
        $currentIds[] = $term->term_id;
    }

    $terms = get_terms([
        'taxonomy' => 'kmcategory',
        'hide_empty' => false
    ]);

    echo '<div class="f-row genres">';

    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
        foreach ( $terms as $term ) {
            $term_id = $term->term_id;
            $checked = in_array($term_id, $currentIds) ? 'checked' : '';

            echo '<input type="checkbox" name="_movie_genre[]" value="'.esc_html($term_id).'" '.$checked.' /> '.esc_html($term->name).'<br>';

        }
    }

    echo '</div>';
    wp_nonce_field( '_movie_fields_nonce', '_movie_fields_process' );
}