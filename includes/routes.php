<?php

require_once(ABSPATH . 'wp-admin/includes/media.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
require_once(ABSPATH . 'wp-admin/includes/image.php');

// Routes
add_action( 'rest_api_init', function () {
    register_rest_route( 'kmovies/v1', '/send-email', array(
        'methods' => WP_REST_Server::CREATABLE,
        'callback' => 'kmovies_send_email',
    ) );

    register_rest_route( 'kmovies/v1', '/load-more', array(
        'methods' => WP_REST_Server::CREATABLE,
        'callback' => 'kmovies_load_more',
    ) );

    register_rest_route( 'kmovies/v1', '/import-movies', array(
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'kmovies_import_movies',
    ) );

    register_rest_route( 'kmovies/v1', '/import-genres', array(
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'kmovies_import_genres',
    ) );
} );

// Send email function
function kmovies_send_email( $request){

    $to = get_bloginfo('admin_email');
    $subject = __('Quote from visitor', 'kmovies-plugin');
    
    $body = __('Movie title:', 'kmovies-plugin') .' ' . sanitize_text_field( $request['title']) . '<br>';
    $body .= __('Message:', 'kmovies-plugin') .' ' . sanitize_text_field( $request['message'] ) . '<br>';
    $body .= __('From Name:', 'kmovies-plugin') .' ' . sanitize_text_field( $request['name'] ).  '<br>';
    $body .= __('From Email:', 'kmovies-plugin') .' ' . sanitize_text_field( $request['email'] ) . '<br>';
    $headers = array('Content-Type: text/html; charset=UTF-8');

    $mailResult = wp_mail( $to, $subject, $body, $headers );

    if ( $mailResult ) {
        $message = __('Email sent successfully!', 'kmovies-plugin');
    }else{
        $message = __('Email sending failed!', 'kmovies-plugin');
    }

    return new WP_REST_Response( $message, 200 );
}

// Load more function
function kmovies_load_more( $request){

    $page = (int)$request['paged'];
    $ajaxposts = new WP_Query([
        'post_type' => 'kmovies',
        'posts_per_page' => 10,
        'orderby' => 'date',
        'order' => 'DESC',
        'paged' => $page,
    ]);
  
    $html = '';
  
    if($ajaxposts->have_posts()) {
        while($ajaxposts->have_posts()) : $ajaxposts->the_post();
            $html .= '<li>';
            $html .= '<a class="movie-popup" href="' . esc_url(get_the_permalink()) . '">' . esc_html(get_the_title()) . '</a>';
            $html .= '<div class="movie-details">';
            $html .= '<div class="movie-title">'.__('Movie Title', 'kmovies-plugin'). ': ' . get_the_title() . '</div>';
            $html .= '<div class="movie-description">'.__('Movie Description', 'kmovies-plugin'). ': ' . get_the_content() . '</div>';
            $html .= '<div class="movie-trailer">'.__('Movie Trailer', 'kmovies-plugin'). ': ' . get_post_meta(get_the_ID(), '_movie_trailer', true) . '</div>';
            $html .= '<div class="movie-raiting">'.__('Movie Raiting', 'kmovies-plugin'). ': ' . get_post_meta(get_the_ID(), '_movie_raiting', true) . '</div>';
            $html .= '<div class="movie-cast">'.__('Movie Cast', 'kmovies-plugin'). ': ' . get_post_meta(get_the_ID(), '_movie_cast', true) . '</div>';
            $html .= '<div class="movie-release">'.__('Movie Release Date', 'kmovies-plugin'). ': ' . get_post_meta(get_the_ID(), '_movie_release', true) . '</div>';
            $html .= '<div class="movie-thumb">'.__('Movie Poster', 'kmovies-plugin'). ': ' . get_the_post_thumbnail(get_the_ID(), 'thumbnail') . '</div>';
            $html .= do_shortcode('[kmovies_form]');
            $html .= '</div>';
            $html .= '</li>';
        endwhile;
    } else {
        $html = '<div style="color:red;">'.__("No more posts...", "kmovies-plugin").'</div>';
    }
  
    return new WP_REST_Response( $html, 200 );
}

// Import movies
function kmovies_import_movies() {
    global $wpdb;

    $token = sanitize_text_field( get_option('kmovies_mdb_key') );
    if( !$token ) {
        return __('No token found', 'kmovies-plugin');
    }

    // max page is 500 per their API. Alternative is recurresion until "success": false response
    for($page = 1; $page <= 10; $page++) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.themoviedb.org/3/movie/popular?language=en-US&page=' . $page,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer ' . $token,
            'accept: application/json'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        if( $response === false ) {
            return "Curl error: " . curl_error($curl);
        }
        $response = json_decode($response, true);
    
        foreach($response['results'] as $movie) {
    
            $movie_title = '';
            if(isset($movie['title'])) {
                $movie_title = $movie['title'];
            }
    
            $movie_poster = '';
            if(isset($movie['poster_path'])){
                $movie_poster = $movie['poster_path'];
            }
    
            $movie_release_date = '';
            if(isset($movie['release_date'])){
                $movie_release_date = $movie['release_date'];
            }
    
            $movie_vote_average = '';
            if(isset($movie['vote_average'])){
                $movie_vote_average = $movie['vote_average'];
            }
    
            $movie_video = '';
            if(isset($movie['video'])){
                $movie_video = $movie['video'];
            }
    
            $genre_ids = [];
            if(isset($movie['genre_ids'])){
                $genre_ids = $movie['genre_ids'];
            }

            $movide_mdb_id = '';
            if(isset($movie['id'])){
                $movide_mdb_id = $movie['id'];
            }

            // avoid duplicates
            $existing_movies = $wpdb->get_results( "
                SELECT *
                FROM  $wpdb->postmeta
                    WHERE meta_key = '_movie_mdb_id'
                    AND meta_value = $movide_mdb_id
                    LIMIT 10;
            " );
            if (count($existing_movies)> 0){
              continue;
            }

            // movie cast request
            $curl1 = curl_init();
                
            curl_setopt_array($curl1, array(
              CURLOPT_URL => 'https://api.themoviedb.org/3/movie/'.$movide_mdb_id.'/credits?language=en-US',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_SSL_VERIFYPEER => false,
              CURLOPT_SSL_VERIFYHOST => false,
              CURLOPT_CUSTOMREQUEST => 'GET',
              CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $token,
                'accept: application/json'
              ),
            ));
            
            $response_cast = curl_exec($curl1);
            
            curl_close($curl1);
            if( $response_cast === false ) {
                return "Curl error: " . curl_error($curl1);
            }
            $response_cast = json_decode($response_cast, true);
            // end movie cast request

            $movie_cast = [];
            $movie_response_cast = [];
            if(isset($response_cast['cast'])){
                $movie_response_cast = $response_cast['cast'];
            }
            foreach($movie_response_cast as $cast) {
                $movie_cast[] = $cast['name'];
            }
    
            $movie = array(
                'post_title' => $movie_title,
                'post_content' => $movie['overview'],
                'post_status' => 'publish',
                'post_type' => 'kmovies',
                'meta_input' => array(
                    '_movie_mdb_id' => $movide_mdb_id,
                    '_movie_trailer' => $movie_video,
                    '_movie_raiting' => $movie_vote_average,
                    '_movie_release' => $movie_release_date,
                    '_movie_cast' => implode(",", $movie_cast)
                )
            );
    
            $movie_id = wp_insert_post($movie);
  
    
            $image = 'https://image.tmdb.org/t/p/w500' . $movie_poster;

            $attachment_id = media_sideload_image($image, $movie_id, null, 'id');
    
            set_post_thumbnail($movie_id, $attachment_id); 
    
            $term_ids = [];
            foreach($genre_ids as $gid) {
                
    
                $result = $wpdb->get_results ( "
                    SELECT term_id
                    FROM  $wpdb->termmeta
                        WHERE meta_key = 'kmovies_mdb_id'
                        AND meta_value = $gid
                        LIMIT 1;
                " );
                
                foreach ( $result as $term )
                {
                  array_push($term_ids, $term->term_id);
                }
            }
            wp_set_post_terms($movie_id, $term_ids, 'kmcategory', false);
        }
    }
    return __('Movies imported', 'kmovies-plugin');
}

// import genres to movie categories
function kmovies_import_genres() {
    $token = sanitize_text_field( get_option('kmovies_mdb_key') );
    if( !$token ) {
      return __('No token found', 'kmovies-plugin');
    }
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://api.themoviedb.org/3/genre/movie/list?language=en',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Authorization: Bearer ' . $token,
        'accept: application/json'
      ),
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);
    if( $response === false ) {
        return "Curl error: " . curl_error($curl);
    }
    $response = json_decode($response, true);
    foreach ($response['genres'] as $value) {
        $term = get_term_by('name', $value['name'], 'kmcategory');
        if( !$term ) {
            $term_id = wp_insert_term($value['name'], 'kmcategory');
            add_term_meta($term_id['term_id'], 'kmovies_mdb_id', $value['id']);
        }
    }
    return __('Genres imported', 'kmovies-plugin');
}