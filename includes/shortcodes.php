<?php

// Shortcode for email form
function kmovies_form() { 

    $url = get_site_url().'/wp-json/kmovies/v1/send-email';

    $html = ''; 

    $html .= '<div id="kmovies_form_wrapper">';
    $html .= '<div id="kmovies-errors"></div>';
    $html .= '<form id="kmovies_form" data-url="'. esc_url( $url ).'">';
    $html .= '<div class="f-row">';
    $html .= '<label for="kmovies_form_name">'.__('Name', 'kmovies-plugin').'</label>';
    $html .= '<input type="text" id="kmovies_form_name" name="kmovies_form_name" placeholder="'.__('Your First and Last Name', 'kmovies-plugin').'">';
    $html .= '</div>';
    $html .= '<div class="f-row">';
    $html .= '<label for="kmovies_form_email">'.__('Your Email', 'kmovies-plugin').'</label>';
    $html .= '<input type="email" id="kmovies_form_email" name="kmovies_form_email" placeholder="'.__('Your Email', 'kmovies-plugin').'">';
    $html .= '</div>';
    $html .= '<div class="f-row">';
    $html .= '<label for="kmovies_form_title">'.__('Movie Title', 'kmovies-plugin').'</label>';
    $html .= '<input type="text" id="kmovies_form_title" name="kmovies_form_title" placeholder="'.__('Movie Title', 'kmovies-plugin').'">';
    $html .= '</div>';
    $html .= '<div class="f-row">';
    $html .= '<label for="kmovies_form_comment">'.__('Your Comment', 'kmovies-plugin').'</label>';
    $html .= '<textarea id="kmovies_form_comment" name="kmovies_form_comment" placeholder="'.__('Your Comment', 'kmovies-plugin').'"></textarea>';
    $html .= '</div>';
    $html .= '<div class="f-row">';
    $html .= '<button type="submit">'.__('Submit', 'kmovies-plugin').'</button>';
    $html .= '</div>';
    $html .= '</form>';
    $html .= '</div>';
        
    return $html;
}
add_shortcode('kmovies_form', 'kmovies_form');


// Shortcode for Movie list
function kmovies_list_shortcode() {
    $movies = new WP_Query([
        'post_type' => 'kmovies',
        'posts_per_page' => 10,
        'orderby' => 'date',
        'order' => 'DESC',
        'paged' => 1,
      ]);

    $html = '';

    $url = get_site_url().'/wp-json/kmovies/v1/load-more';
    
    if($movies->have_posts()):
        $html .= '<ul class="publication-list">';
        while ($movies->have_posts()): $movies->the_post();
            $html .= '<li>';
            $html .= '<a class="movie-popup" href="' . esc_url(get_the_permalink()) . '">' . esc_html(get_the_title()) . '</a>';
            $html .= '<div class="movie-details">';
            $html .= '<div class="movie-title">'.__('Movie Title', 'kmovies-plugin'). ': ' . get_the_title() . '</div>';
            $html .= '<div class="movie-description">'.__('Movie Description', 'kmovies-plugin'). ': ' . get_the_content() . '</div>';
            $html .= '<div class="movie-trailer">'.__('Movie Trailer', 'kmovies-plugin'). ': ' . get_post_meta(get_the_ID(), '_movie_trailer', true) . '</div>';
            $html .= '<div class="movie-raiting">'.__('Movie Raiting', 'kmovies-plugin'). ': ' . get_post_meta(get_the_ID(), '_movie_raiting', true) . '</div>';
            $html .= '<div class="movie-cast">'.__('Movie Cast', 'kmovies-plugin'). ': ' . get_post_meta(get_the_ID(), '_movie_cast', true) . '</div>';
            $html .= '<div class="movie-release">'.__('Movie Release Date', 'kmovies-plugin'). ': ' . get_post_meta(get_the_ID(), '_movie_release', true) . '</div>';
            $html .= '<div class="movie-thumb">'.__('Movie Poster', 'kmovies-plugin'). ': ' . get_the_post_thumbnail(get_the_ID(), 'thumbnail') . '</div>';
            $html .= '<div class="movie-form">' . do_shortcode('[kmovies_form]') . '</div>';
            $html .= '</div>';
            $html .= '</li>';
        endwhile;
        $html .= '</ul>';
    endif;

    $html .= '<div class="btn__wrapper">';
    $html .= '<a href="#!" data-url="'.esc_url( $url ).'" class="btn btn__primary" id="load-more">'.__('Load more', 'kmovies-plugin').'</a>';
    $html .= '</div>';

    return $html;
}
add_shortcode('kmovies_list', 'kmovies_list_shortcode');