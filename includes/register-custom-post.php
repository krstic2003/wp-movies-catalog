<?php

// Register custom post type - Movies
function kmovies_custom_post_type() {
	register_post_type('kmovies',
		array(
			'labels'            => array(
				'name'          => __('Movies', 'kmovies-plugin'),
				'singular_name' => __('Movie', 'kmovies-plugin'),
                'add_new'       => __('Add New Movie', 'kmovies-plugin'),
                'add_new_item'  => __('Add New Movie', 'kmovies-plugin'),
                'new_item'      => __('New Movie', 'kmovies-plugin'),
                'edit_item'     => __('Edit Movie', 'kmovies-plugin'),
                'view_item'     => __('View Movie', 'kmovies-plugin'),
                'all_items'     => __('All Movies', 'kmovies-plugin'),
                'search_items'  => __('Search movies', 'kmovies-plugin'),
			),
            'public'            => true,
            'has_archive'       => true,
            'show_in_rest'      => true,
            'supports'          => array('title', 'editor', 'thumbnail'),
		)
	);
}
add_action('init', 'kmovies_custom_post_type');

// Register custom taxonomy for Movie categories
function kmovies_register_taxonomy_kmcategory() {
    $labels = array(
        'name'              => _x( 'Movie Categories', 'kmovies-plugin' ),
        'singular_name'     => _x( 'Movie Category', 'kmovies-plugin' ),
        'search_items'      => __( 'Search Movie Categories', 'kmovies-plugin' ),
        'all_items'         => __( 'All Movie Categories', 'kmovies-plugin' ),
        'parent_item'       => __( 'Parent Movie Category', 'kmovies-plugin' ),
        'parent_item_colon' => __( 'Parent Movie Category:', 'kmovies-plugin' ),
        'edit_item'         => __( 'Edit Movie Category', 'kmovies-plugin' ),
        'update_item'       => __( 'Update Movie Category', 'kmovies-plugin' ),
        'add_new_item'      => __( 'Add New Movie Category', 'kmovies-plugin' ),
        'new_item_name'     => __( 'New Movie Category Name', 'kmovies-plugin' ),
        'menu_name'         => __( 'Movie Categories', 'kmovies-plugin' ),
    );
    $args   = array(
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => [ 'slug' => 'kmcategory' ],
    );
    register_taxonomy( 'kmcategory', [ 'kmovies' ], $args );
}
add_action( 'init', 'kmovies_register_taxonomy_kmcategory' );