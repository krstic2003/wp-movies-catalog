# wp-movies-catalog

## Installation and instructions 

1. Upload the entire 'krstic-movies' folder to the '/wp-content/plugins/' directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. You will find 'Movies Catalog' menu item in your WordPress admin panel. There you can adjust plugin options (set API key) and see availabe shortcodes.
    YOUR-URL/wp-admin/admin.php?page=kmovies_page
4. In order to display move list insert the following shortcode on any page - [kmovies_list]
5. In order to display email form, insert the following shortcode on any page - [kmovies_form]

***[kmovies_list] contains [kmovies_form] already!

## Importers

1. There are manual importer buttons in plugin settings page - YOUR-URL/wp-admin/admin.php?page=kmovies_page
*** Import first generes, then movies please
2. There are also cron jobs scheduled. Generes at midnight. Movies import will start 30 mins after.

## Should be done in future for better exectution of import

1. Movie import should be splitten in smaller "chunks"