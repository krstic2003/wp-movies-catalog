jQuery(document).ready(function($){

    // Submit form
    $("#kmovies_form").on("submit", function(event){
        event.preventDefault();
        
        var name = $("#kmovies_form_name").val();
        var email = $("#kmovies_form_email").val();
        var title = $("#kmovies_form_title").val();
        var comment = $("#kmovies_form_comment").val();
        var url = $("#kmovies_form").attr('data-url');

        var formdata = new FormData();
        formdata.append("email", email);
        formdata.append("title", title);
        formdata.append("message", comment);
        formdata.append("name", name);

        var requestOptions = {
            method: "POST",
            body: formdata,
            redirect: "follow"
        };
    
        fetch(url, requestOptions)
            .then(response => response.text())
            .then(result => alert(result))
            .catch(error => $("#kmovies-errors").html(error));
        
    });

    // Movie list load more
    let currentPage = 1;
    $('#load-more').on('click', function(e) {
        e.preventDefault();
        currentPage++; // Do currentPage + 1, because we want to load the next page

        var formdata = new FormData();
        formdata.append("paged", currentPage);
        var url = $(this).attr('data-url');

        var requestOptions = {
            method: "POST",
            body: formdata,
            redirect: "follow"
        };
    
        fetch(url, requestOptions)
            .then(function (response) {
                if (response.status !== 200) {
                console.log(
                    "Looks like there was a problem. Status Code: " + response.status
                );
                return;
                }
                response.json().then(function (data) {
                    console.log(data);
                    $('.publication-list').append(data);
                });
            })
            .catch(error => $('.publication-list').append(error));

    });

    // Movie details popup
    $(document).on("click", "a.movie-popup", function(e){
        e.preventDefault();
        var popup = $(this).closest('li').find('.movie-details');
        popup.dialog({
            resizable: false,
            width: 900,
            height: 800,
            modal: true,
            open: function( event, ui ) {
               $(this).find("#kmovies_form_title").val($(this).find('.movie-title').text()).change();
            }
        });
        
    })

})