<?php
/**
* Plugin Name: Movies catalog
* Description: Movies catalog based on themoviedb.org
* Version: 1.0
* Author: Aleksandar Krstic
* Author URI: http://www.krstic.info/
* Text Domain: kmovies-plugin
**/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


require_once( 'includes/settings.php' );
require_once( 'includes/shortcodes.php' );
require_once( 'includes/routes.php' );
require_once( 'includes/register-custom-post.php' );
require_once( 'includes/custom-fields.php' );
require_once( 'includes/crons.php' );

// Admin CSS and JS
function kmovies_admin_style() {
    wp_enqueue_script( 'jquery-ui-core' );
    wp_enqueue_script('jquery-ui-sortable');

    wp_register_script( 'kmovies-custom-js', plugin_dir_url( __FILE__ ) . 'assets/js/custom.js', array('jquery-core'), false, true );
    wp_enqueue_script( 'kmovies-custom-js' );

    wp_register_style( 'kmovies_admin_css', plugin_dir_url( __FILE__ ) . 'assets/css/admin-style.css', false, '1.0.0' );
    wp_enqueue_style( 'kmovies_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'kmovies_admin_style' );

// Front CSS and JS
function kmovies_front_styles() {
    wp_enqueue_script( 'jquery-ui-core' );
    wp_enqueue_script('jquery-ui-dialog');

    wp_register_script( 'kmovies-custom-front-js', plugin_dir_url( __FILE__ ) . 'assets/js/custom-front.js', array('jquery')  );
    wp_enqueue_script( 'kmovies-custom-front-js' );

    wp_register_style( 'kmovies_front_css', plugin_dir_url( __FILE__ ) . 'assets/css/style-front.css'  );
    wp_enqueue_style( 'kmovies_front_css' );

}
add_action( 'wp_enqueue_scripts', 'kmovies_front_styles' );


// Activate the plugin.
function kmovies_activate() { 
	// Trigger our function that registers the custom post type plugin.
	kmovies_custom_post_type(); 
    kmovies_register_taxonomy_kmcategory();
	// Clear the permalinks after the post type has been registered.
	flush_rewrite_rules(); 
}
register_activation_hook( __FILE__, 'kmovies_activate' );

// Deactivation hook.
function kmovies_deactivate() {
	// Unregister the post type, so the rules are no longer in memory.
	unregister_post_type( 'kmovies' );
    unregister_taxonomy( 'kmcategory');
	// Clear the permalinks to remove our post type's rules from the database.
	flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'kmovies_deactivate' );

